### LinkMagic Project

---

#### We'll be using:

* [Laravel Framework](https://laravel.com/)
* [Laravel Sail](https://laravel-news.com/laravel-sail)
* [Laravel Livewire](https://laravel-livewire.com/)
* [Laravel Pint](https://laravel-news.com/laravel-pint)
* [Larastan](https://laravel-news.com/running-phpstan-on-max-with-laravel)
* [JSON-API Resources](https://github.com/timacdonald/json-api)
* [Laravel Query Builder](https://laravel-news.com/laravel-query-builder)
* [Fast Paginate](https://laravel-news.com/laravel-fast-paginate)
* [Data Object Tools](https://github.com/JustSteveKing/laravel-data-object-tools)

#### Models:

```yaml
Users:
  attributes:
    default Sanctum: yes
  relations:
    
Roles:
  attributes:
    id: int
    role_type_id: int
    timestamps: yes
    softdeletes: yes
  relations:
    role_types: belongsTo

RoleTypes:
  attributes:
    id: int
    role_type: string
  relations:
    roles: belongsToMany

Clients:
  attributes:
    id: int
    email: string
    password: string
    phone: string
    country_code: string
    timestamps: yes
    softdeletes: yes
  relations:
    orders: belongsToMany
    projects: belongsToMany
    reports: belongsToMany
    payments: belongsToMany
    notes: belongsToMany

Subscriptions:
  attributes:
    id: int
    status: smallint
    timestamps: yes
    softdeletes: yes
  relations:
    orders: BelongToMany
    payment_history: BelongTo

Orders:
  attributes:
    id: int
    no: string
    status: smallint
    promo: string
    subscription_id: int
    timestamps: yes
    softdeletes: yes
  relations:
    clients: BelongTo
    notes: BelongToMany
    payments: BelongToMany

Projects:
  attributes:
    id: int
    slug: string
    tags: string
    order_id: int
    client_id: int
    ссылки с ресурсов: !need details
    ресурсы на индексации: !need details
    timestamps: yes
    softdeletes: yes
  relations:
    clients: BelongToMany
    orders: BelongTo

Payments:
  attributes:
    id: int
    client_id: int
    order_id: int
    status: smallint
    payment_type_id: int
    subscription_id: int
    amount: int
    timestamps: yes
    softdeletes: yes
  relations:
    clients: BelongToMany
    
PaymentTypes:
  attributes:
    id: int
    amount: int
    duration: int
    timestamps: yes
    softdeletes: yes
  relations:
    payment_history: BelongToMany
    
SupportNotes:
  attributes:
    id: int
    client_id: int
    order_id: int
    timestamps: yes
    softdeletes: yes
  relations:
    clients: BelongTo
    orders: BelongTo

Reports:
  attributes:
    id: int
    client_id: int
    order_id: int
    project_id: int
    status: smallint
    timestamps: yes
    softdeletes: yes
  relations:
    clients: BelongTo
    orders: BelongTo

ErrorLog:
  attributes:
    id: int
    entity_id: int
    error_code: int
    error_text: string
    timestamps: yes
  relations:
    clients: BelongTo
    orders: BelongTo

MailTemplates:
  attributes:
    id: int
    subject: string
    body: text
    delivery_id: int
    timestamps: yes
    softdeletes: yes
  relations:
    delivery_types: BelongTo

DeliveryTypes:
  attributes:
    id: int
    title: string
    template_id: int
    frequency_type: int
    delivery_count: int (Q-а если ежемесячно, как использовать кол-во отправок)
    filter_id: int
    timestamps: yes
    softdeletes: yes
  relations:
    mail_templates: BelongToMany

```

#### Web Admin Project Estimations: 30d-35d, 240h-280h

* User`s Management, 2d:
  - models, migrations, factories (Users, Roles)
  - forms' design (Registration, Login, Password Change, etc.)
  - registration:
    - email verification
    - password change
  - authentication
  - authorization
  - CRUD for models
  - define actions for authorization
* Main Page, 1d:
  - Design Main page with Menu for general Entities (Clients, Support, Orders, Projects, Payments, etc.)
  - Forms' stubs for these entities
* Clients, 3d:
  - models, migrations, factories (Customers, Orders, Projects, Payments, Support Notes)
  - API routes
  - CRUD Controller
  - sorting, filtering, searching
  - Forms' Design and Integration
* Interactions/Support, 3d:
  - models, migrations, factories
  - API routes
  - CRUD
  - sorting, filtering, searching
  - Forms` Design and Integration
* Projects, 3d:
  - models, migrations, factories
  - API routes
  - CRUD
  - sorting, filtering, searching
  - Forms` Design and Integration
* Orders, 3d:
  - models, migrations, factories
  - API routes
  - CRUD
  - sorting, filtering, searching
  - Forms` Design and Integration
* Payments, 3d:
  - models, migrations, factories
  - API routes
  - sorting, filtering, searching
  - Forms` Design and Integration
* Reports, 3d:
  - models, migrations, factories
  - API routes
  - sorting, filtering, searching
  - Forms` Design and Integration
* Promos, 3d:
  - models, migrations, factories
  - API routes
  - sorting, filtering, searching
  - Forms' Design and Integration
* Deliveries: 5d-10d
  - models, migrations, factories
  - API routes
  - sorting, filtering, searching
  - Forms' Design and Integration
  - Queues, Jobs
  - Load Testing

#### Bot Project Estimation: 5-10d

#### Publication Job Service Estimation: 5d

### Total for Project: 40-45d, 320-360h
  
#### Hour rate: $18
